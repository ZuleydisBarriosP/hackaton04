import { useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";

import * as ClinicaServer from "../server/ClinicaServer";

const Formulario = () => {
const navigate = useNavigate();
const params = useParams();

console.log(params);
  const initialState = {
    nombres: "",
    apellidos: "",
    dni: "",
    direccion: "",
    correo: "",
    telefono: "",
    sexo: "",
    numColegiatura: "",
    especialidad: "",
    fechaNacimiento: "",
    fechaRegistro: "",
    fechaModificacion: "",
    usuarioRegistro: "",
    usuarioModificacion: "",
    activo: true,
  };


  const [formulario, setFormulario] = useState(initialState);

  const handleInputChange = (e) => {
    setFormulario({...formulario, [e.target.name]: e.target.value});
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    /* console.log(formulario); */
    try {
      let res;
      if (!params.id) {
        res = await ClinicaServer.registerMedico(formulario);
        const data = await res.json();
        if (res.status === 201) {
          setFormulario(initialState);
        }
      }else{
        await ClinicaServer.updateMedico(params.id, formulario);       
      }

      navigate("/medicos");
   
    } catch (error) {
      console.log(error);
    }
  }


  const getMedico = async (medicoId) =>{
    try{
      const res = await ClinicaServer.getMedico(medicoId);
      const data = await res.json();
      const {nombres, apellidos, dni, direccion, correo, telefono, sexo, numColegiatura, especialidad, fechaNacimiento, fechaRegistro, fechaModificacion, usuarioRegistro, usuarioModificacion, activo} = data;
      setFormulario({nombres, apellidos, dni, direccion, correo, telefono, sexo, numColegiatura, especialidad, fechaNacimiento, fechaRegistro, fechaModificacion, usuarioRegistro, usuarioModificacion, activo});
      
    }
  catch(error){
    console.log(error);
  }
  }

  useEffect(() => {
    if (params.id) {
      getMedico(params.id);
    }
  }, []);
  


  return (
    <>
      <h1 className="text-center mb-5">Formulario Medicos</h1>

      <form onSubmit={handleSubmit}>
        <div className="mb-3">
          <label htmlFor="id_nombres" className="form-label">
            Nombres
          </label>
          <input
            type="text"
            className="form-control"
            id="id_nombres"
            name="nombres"
            value={formulario.nombres}
            onChange={handleInputChange}
            aria-describedby="textlHelp"
            placeholder="Ingrese sus nombres"
            maxLength={50}
            required=""
          />
        </div>

        <label htmlFor="id_apellidos" className="form-label">
          Apellidos
        </label>
        <input
          type="text"
          className="form-control"
          name="apellidos"
          value={formulario.apellidos}
          onChange={handleInputChange}
          aria-describedby="textlHelp"
          placeholder="Ingrese sus apellidos"
          maxLength={50}
          required=""
          id="id_apellidos"
        />

        <label htmlFor="id_dni" className="form-label">
          DNI
        </label>
        <input
          type="number"
          className="form-control"
          name="dni"
          value={formulario.dni}
          onChange={handleInputChange}
          aria-describedby="textlHelp"
          placeholder="Ingrese DNI"
          maxLength={50}
          required=""
          id="id_dni"
        />

        <label htmlFor="id_direccion" className="form-label">
          Direccion
        </label>
        <input
          type="text"
          className="form-control"
          name="direccion"
          value={formulario.direccion}
          onChange={handleInputChange}
          aria-describedby="textlHelp"
          placeholder="Ingrese su direccion"
          maxLength={50}
          required=""
          id="id_direccion"
        />

        <div className="mt-3 mb-3">
          <label htmlFor="id_correo" className="form-label">
            Email address
          </label>
          <input
            type="email"
            className="form-control"
            name="correo"
            value={formulario.correo}
            onChange={handleInputChange}
            aria-describedby="emailHelp"
            maxLength={50}
            required=""
            id="id_correo"
          />
        </div>

        <label htmlFor="id_telefono" className="form-label">
          Telefono
        </label>
        <input
          type="number"
          className="form-control"
          aria-describedby="textlHelp"
          placeholder="Ingrese Telefono"
          name="telefono"
          value={formulario.telefono}
          onChange={handleInputChange}
          maxLength={50}
          required=""
          id="id_telefono"
        />

        <label htmlFor="exampleText" className="form-label">
          Sexo
        </label>
        <input
          type="text"
          className="form-control"
          name="sexo"
          value={formulario.sexo}
          onChange={handleInputChange}
          aria-describedby="textlHelp"
          placeholder="Ingrese su sexo"
          maxLength={10}
          required=""
          id="id_sexo"
        />

        <label htmlFor="id_sexo" className="form-label">
          Numero de Colegiatura
        </label>
        <input
          type="number"
          className="form-control"
          name="numColegiatura"
          value={formulario.numColegiatura}
          onChange={handleInputChange}
          aria-describedby="textlHelp"
          placeholder="Numero de Colegiatura"
          id="id_numColegiatura"
        />

        <label htmlFor="exampleText" className="form-label">
          Fecha Nacimiento
        </label>
        <input
          type="date"
          name="fechaNacimiento"
          value={formulario.fechaNacimiento}
          onChange={handleInputChange}
          className="form-control"
          required=""
          id="id_fechaNacimiento"
        />

        <label htmlFor="exampleText" className="form-label">
          Fecha Registro
        </label>
        <input
          type="date"
          name="fechaRegistro"
          value={formulario.fechaRegistro}
          onChange={handleInputChange}
          className="form-control"
          size={10}
          required=""
          id="id_fechaRegistro"
        />

        <label htmlFor="exampleText" className="form-label">
          Fecha Modificacion
        </label>
        <input
          type="date"
          className="form-control"
          name="fechaModificacion"
          value={formulario.fechaModificacion}
          onChange={handleInputChange}
          size={10}
          required=""
          id="id_fechaModificacion"
        />

        <label htmlFor="exampleText" className="form-label">
          Usuario Registro
        </label>
        <input
          type="text"
          className="form-control"
          name="usuarioRegistro"
          value={formulario.usuarioRegistro}
          onChange={handleInputChange}
          size={10}
          required=""
          id="id_usuarioRegistro"
        />

        <label htmlFor="exampleText" className="form-label">
          Usuario Modificacion
        </label>
        <input
          type="text"
          className="form-control"
          name="usuarioModificacion"
          value={formulario.usuarioModificacion}
          onChange={handleInputChange}
          size={10}
          required=""
          id="id_usuarioModificacion"
        />

        {params.id ? (
          <button
            type="submit"
            className="btn btn-primary mt-4 mb-4"
            defaultValue="Save"
            name="_save"
            id="id_save"
          >
            Actualizar
          </button>
        ) : (
          <button
            type="submit"
            className="btn btn-primary mt-4 mb-4"
            defaultValue="Save"
            name="_save"
            id="id_save"
          >
            Enviar
          </button>
        )}
      </form>
    </>
  );
};
export default Formulario;
