import { Formik, Field, Form } from "formik";
function FomConFormik() {
  return (
    <div>
        <h1>Formulario Medicos</h1>
        <Formik
                initialValues={{
                nombres: "",
                apellidos: "",
                dni: "",
                direccion: "",
                email: "",
                telefono: "",
                sexo: "",
                numColegiatura : "",
                especialidad: "",
                fechaNacimiento: "",
                fechaRegistro: "",
                fechaModificacion: "",
                usuarioRegistro: "",
                usuarioModificacion: "",
                activo: false,
            }}
            onSubmit={(
                values) => {
                console.log(values);
                }
            }
        >
            {({ isSubmitting }) => (
                <Form>
                    <Field type="text" name="nombres" className="form-control mb-3" placeholder="Nombres" />
                    <Field type="text" name="apellidos" className="form-control mb-3" placeholder="Apellidos" />
                    <Field type="number" name="dni" className="form-control mb-3" placeholder="DNI" />
                    <Field type="text" name="direccion" className="form-control mb-3" placeholder="Direccion" />
                    <Field type="email" name="email" className="form-control mb-3" placeholder="Email" />
                    <Field type="number" name="telefono" className="form-control mb-3" placeholder="Telefono" />
                    <Field type="text" name="sexo" className="form-control mb-3" placeholder="Sexo" />
                    <Field type="number" name="numColegiatura" className="form-control mb-3" placeholder="Numero Colegiatura" />
                    <Field type="text" name="especialidad" className="form-control mb-3" placeholder="Especialidad" />
                    <Field type="date" name="fechaNacimiento" className="form-control mb-3" placeholder="Fecha Nacimiento" />
                    <Field type="date" name="fechaRegistro" className="form-control mb-3" placeholder="Fecha Registro" />
                    <Field type="date" name="fechaModificacion" className="form-control mb-3" placeholder="Fecha Modificacion" />
                    <Field type="select" name="usuarioRegistro" className="form-control mb-3" placeholder="Usuario Registro" />
                    <Field type="select" name="usuarioModificacion" className="form-control mb-3" placeholder="Usuario Modificacion" />
                    <Field type="checkbox" name="activo" className="checkbox mb-3" placeholder="Activo" />
                    <br />
                    <button type="submit" className="btn btn-primary mb-4" disabled={isSubmitting}>
                        Submit
                    </button>
                </Form>
            )}
            </Formik>
        </div>  
  );
}
export default FomConFormik;