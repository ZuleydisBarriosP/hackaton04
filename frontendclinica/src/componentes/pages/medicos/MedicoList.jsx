import React, { useEffect, useState } from 'react';

//Componentes
import MedicoItem from './MedicoItem';

import * as ClinicaServer from '../../server/ClinicaServer'

const MedicoList = () => {
    const [medicos,setMedicos] = useState([]);

    const listMedicos= async()=>{
        try {
            const res = await ClinicaServer.listMedicos();
            const data = await res.json();            
            setMedicos(data.results)
        } catch (error) {
            console.log(error);        
        }
    }

    useEffect(()=>{
        listMedicos()},[]);

    return (
      <div>
        <h1 className="text-center mb-4 ">Listado Medicos</h1>

        <table className="table text-center">
          <thead>
            <tr>
              <th>Id</th>
              <th>Nombres</th>
              <th>Apellidos</th>
              <th>Dni</th>
              <th>Direccion</th>
              <th>Correo</th>
              <th>Telefono</th>
              <th>Sexo</th>
              <th>Acciones</th>
            </tr>
          </thead>
          {medicos.map((medico) => (
            <MedicoItem
              key={medico.id}
              medicos={medico}
              listMedicos={listMedicos}
            />
          ))}
        </table>
      </div>
    );
};

export default MedicoList;
