import React from 'react';
import { useNavigate } from "react-router-dom";


import * as ClinicaServer from "../../server/ClinicaServer";



const MedicoItem=({medicos, listMedicos})=>{

    const navigate = useNavigate();

    const handleDelete= async (medicoId)=>{
        await ClinicaServer.deleteMedico(medicoId);
        listMedicos();
    };
    return(
        <tbody>
                <tr>
                    <td scope="row">{medicos.id}</td>
                    <td>{medicos.nombres}</td>
                    <td>{medicos.apellidos}</td>
                    <td>{medicos.dni}</td>
                    <td>{medicos.direccion}</td>
                    <td>{medicos.correo}</td>
                    <td>{medicos.telefono}</td>
                    <td>{medicos.sexo}</td>                  
                    <td><button className="btn" onClick={()=>navigate(`/updateMedico/${medicos.id}`) }><span className="badge bg-success">Editar</span>
                    </button></td>
                    <td><button className="btn" onClick={()=>medicos.id && handleDelete(medicos.id) }><span className="badge bg-danger">Eliminar</span>
                    </button></td>
                </tr>
            </tbody>
        
    );

};
export default MedicoItem;