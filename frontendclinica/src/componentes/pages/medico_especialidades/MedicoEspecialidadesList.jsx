import React, {useState, useEffect} from "react";

import * as MedicoEspecialidadServer from "../../server/MedicoEspecialidadServer";
import MedicoEspecialidadesItem from "./MedicoEspecialidadesItem";


const MedicoEspecialidadesList=()=>{
    const [medicoEspecialidades, setMedicoEspecialidades] = useState([]);
    

    const listMedicoEspecialidades= async ()=>{
        try {
            const res = await MedicoEspecialidadServer.listMedicoEspecialidades();
            const data = await res.json();
            setMedicoEspecialidades(data.results);
        } catch (error) {
            console.log(error);
        }            
    }

    useEffect(()=>{
        listMedicoEspecialidades();
    },[]);


    
    return (
      <div>
        <h1 className="text-center mb-4">Listado de Medico Especialidades</h1>
        <table className="table text-center">
          <thead>
            <tr>
              <th>Id</th>
              <th>Medico</th>
              <th>Especialidad</th>
              <th>Fecha de Registro</th>
              <th>Fecha de Modificacion</th>
              <th>Usuario Registro</th>
              <th>Usuario Modificacion</th>            
              <th>Acciones</th>
            </tr>
          </thead>
          {medicoEspecialidades.map((medicoEspecialidad) => (
            <MedicoEspecialidadesItem key= {medicoEspecialidad.id} medicoEspecialidades={medicoEspecialidad} listMedicoEspecialidades= {listMedicoEspecialidades}/>
          ))}
        </table>
      </div>
    );
}
export default MedicoEspecialidadesList;

