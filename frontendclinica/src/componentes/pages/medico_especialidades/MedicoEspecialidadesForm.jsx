import { useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";


import * as MedicoEspecialidadServer from "../../server/MedicoEspecialidadServer";
import * as ClinicaServer from "../../server/ClinicaServer";
import * as EspecialidadesServer from "../../server/EspecialidadesServer";


const MedicoEspecialidadesForm = () => {

const navigate = useNavigate();
const params = useParams();

  const initialState = {
    medicoId: "",
    especialidadId: "",
    fechaRegistro: "",
    fechaModificacion: "",
    usuarioRegistro: "1",
    usuarioModificacion: "1",
    activo : true,    
  };

  const [medicoEspecialidadesForm, setMedicoEspecialidadesForm] = useState(initialState);
  const [medicos, setMedicos] = useState([]);
  const [especialidades, setEspecialidades] = useState([]);

  const handleInputChange = (e) => {
    setMedicoEspecialidadesForm({ ...medicoEspecialidadesForm, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
        let res;
        if (!params.id) {
          res = await MedicoEspecialidadServer.registerMedicoEspecialidad(medicoEspecialidadesForm);
          const data = await res.json();
          if (res.status === 201) {
            setMedicoEspecialidadesForm(initialState);
          }
        } else {
          await MedicoEspecialidadServer.updateMedicoEspecialidad(params.id, medicoEspecialidadesForm);
        }
      
      navigate("/medicoespecialidadeslist");

    } catch (error) {
      console.log(error);
    }
        
    }


    const listMedicos = async () => {
      try {
        const res = await ClinicaServer.listMedicos();
        const data = await res.json();
        setMedicos(data.results);
      } catch (error) {
        console.log(error);
      }
    };

    useEffect(() => {
      listMedicos();
    }, []);


    const getMedicoEspecialidad = async (medicoEspecialidadId) =>{
    try{      
      const res = await MedicoEspecialidadServer.getMedicoEspecialidad(medicoEspecialidadId);
      const data = await res.json();
      const {  medicoId, especialidadId, fechaRegistro, fechaModificacion, usuarioRegistro, usuarioModificacion, activo} = data;
      setMedicoEspecialidadesForm({medicoId, especialidadId, fechaRegistro, fechaModificacion, usuarioRegistro, usuarioModificacion, activo});      
    }
  catch(error){
    console.log(error);
  }
  }

  useEffect(() => {
    if (params.id) {
      getMedicoEspecialidad(params.id);
    }
  }, []);


   const listEspecialidades = async () => {
     try {
      const res = await EspecialidadesServer.listEspecialidades();
      const data = await res.json();
      setEspecialidades(data.results);
     } catch (error) {
      console.log(error);
     }
   };
    useEffect(() => {
      listEspecialidades();
    }, []);
    
    
 

  return (
    <>
      <h1 className="text-center mb-5">Formulario Especialidades</h1>

      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="">Medico</label>
          <select
            className="form-select"
            aria-label="Default select example"
            name="medicoId"
            value={medicoEspecialidadesForm.medicoId}
            onChange={handleInputChange}
          >
            <option defaultValue="None">Selecciona un medico</option>
            {medicos.map((medico) => {
              return (
                <option key={medico.id} value={medico.id} name="medicoId">
                  {medico.nombres}
                </option>
              );
            })}
          </select>
        </div>

        <div className="form-group">
          <label htmlFor="id_especialidadId">Especialidades</label>
          <select
            className="form-select"
            aria-label="Default select example"
            name="especialidadId"
            value={medicoEspecialidadesForm.especialidadId}
            onChange={handleInputChange}>
            <option defaultValue="None">Selecciona una especialidades</option>
            {especialidades.map((especialidad) => {
              return (
                <option
                  key={especialidad.id}
                  value={especialidad.id}
                  name="especialidadId">
                  {especialidad.nombre}
                </option>
              );
            })}
          </select>
        </div>  

        <label htmlFor="id_fechaRegistro" className="form-label">
          Fecha de Registro
        </label>
        <input
          type="date"
          className="form-control"
          name="fechaRegistro"
          value={medicoEspecialidadesForm.fechaRegistro}
          onChange={handleInputChange}
          aria-describedby="textlHelp"
          placeholder="Ingrese una Fecha de Registro"
          maxLength={50}
          required=""
          id="id_fechaRegistro"
        />

        <label htmlFor="id_fechaModificacion" className="form-label">
          Fecha Modificacion
        </label>
        <input
          type="date"
          className="form-control"
          name="fechaModificacion"
          value={medicoEspecialidadesForm.fechaModificacion}
          onChange={handleInputChange}
          aria-describedby="textlHelp"
          placeholder="Ingrese Fecha Modificacion"
          maxLength={50}
          required=""
          id="id_fechaModificacion"
        />

        <label htmlFor="id_usuarioRegistro" className="form-label">
          Usuario Registro
        </label>
        <input
          type="number"
          className="form-control"
          aria-describedby="textlHelp"
          placeholder="Ingrese usuario Registro"
          name="usuarioRegistro"
          value={medicoEspecialidadesForm.usuarioRegistro}
          onChange={handleInputChange}
          maxLength={50}
          required=""
          id="id_usuarioRegistro"
        />

        <label htmlFor="id_usuarioModificacion" className="form-label">
          Usuario Modificacion
        </label>
        <input
          type="number"
          className="form-control"
          aria-describedby="textlHelp"
          placeholder="Ingrese usuario Modificacion"
          name="usuarioModificacion"
          value={medicoEspecialidadesForm.usuarioModificacion}
          onChange={handleInputChange}
          maxLength={50}
          required=""
          id="id_usuarioModificacion"
        />

        {params.id ? (
          <button
            type="submit"
            className="btn btn-primary mt-4 mb-4"
            defaultValue="Save"
            name="_save"
            id="id_save"
          >
            Actualizar
          </button>
        ) : (
          <button
            type="submit"
            className="btn btn-primary mt-4 mb-4"
            defaultValue="Save"
            name="_save"
            id="id_save"
          >
            Enviar
          </button>
        )}
      </form>
    </>
  );
};
export default MedicoEspecialidadesForm;
