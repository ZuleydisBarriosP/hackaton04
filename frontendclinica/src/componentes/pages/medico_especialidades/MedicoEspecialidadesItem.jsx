import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";


import * as MedicoEspecialidadServer from "../../server/MedicoEspecialidadServer";
import * as ClinicaServer from "../../server/ClinicaServer";
import * as EspecialidadesServer from "../../server/EspecialidadesServer";



const MedicoEspecialidadesItem = ({medicoEspecialidades, listMedicoEspecialidades}) => {

    const navigate = useNavigate();
    const [medicos, setMedicos] = useState([]);

    const handleDelete = async (medicoEspecialidadId) => {
        await MedicoEspecialidadServer.deleteMedicoEspecialidad(medicoEspecialidadId);
        listMedicoEspecialidades();
    };

    const listMedicos = async () => {
          try {
            const res = await ClinicaServer.listMedicos();
            const data = await res.json();
            setMedicos(data.results);
          } catch (error) {
            console.log(error);
          }
        };

        useEffect(() => {
          listMedicos();
        }, []);

        const findMedicos = (idMedico) => {
          return medicos.find((medico) => medico.id === idMedico)?.nombres + " " + medicos.find((medico) => medico.id === idMedico)?.apellidos;
        };

    
    const [especialidades, setEspecialidades] = useState([]);

    const listEspecialidades = async () => {
      try {
        const res = await EspecialidadesServer.listEspecialidades();
        const data = await res.json();
        setEspecialidades(data.results);
      } catch (error) {
        console.log(error);
      }
    };

    useEffect(() => {
      listEspecialidades();
    }, []);


    const findEspecialidades = (idEspecialidad) => {
      return especialidades.find((especialidad) => especialidad.id === idEspecialidad)?.nombre;
    }
    


    return (
      <tbody>
        <tr>
          <td scope="row">{medicoEspecialidades.id}</td>
          <td>{findMedicos(medicoEspecialidades.medicoId)}</td>
          <td>{findEspecialidades(medicoEspecialidades.especialidadId)}</td>
          <td>{medicoEspecialidades.fechaRegistro}</td>
          <td>{medicoEspecialidades.fechaModificacion}</td>
          <td>{medicoEspecialidades.usuarioRegistro}</td>
          <td>{medicoEspecialidades.usuarioModificacion}</td>
          <td>
            <button
              className="btn"
              onClick={() =>
                navigate(
                  `/updateMedicoEspecialidad/${medicoEspecialidades.id}`
                )
              }
            >
              <span className="badge bg-success">Editar</span>
            </button>
          </td>
          <td>
            <button
              className="btn"
              onClick={() =>
                medicoEspecialidades.id && handleDelete(medicoEspecialidades.id)
              }
            >
              <span className="badge bg-danger">Eliminar</span>
            </button>
          </td>
        </tr>
      </tbody>
    );
}
export default MedicoEspecialidadesItem;


