import React from "react";
import { useNavigate } from "react-router-dom";


import * as EspecialidadesServer from "../../server/EspecialidadesServer";



  const EspecialidadesItem = ({especialidades, listEspecialidades}) => {

    const navigate = useNavigate();
    

    const handleDelete = async (especialidadId) => {
        await EspecialidadesServer.deleteEspecialidad(especialidadId);
        listEspecialidades();
    };



    return (
      <tbody>
        <tr>
          <td scope="row">{especialidades.id}</td>
          <td>{especialidades.nombre}</td>
          <td>{especialidades.descripcion}</td>
          <td>{especialidades.fechaRegistro}</td>
          <td>{especialidades.fechaModificacion}</td>
          <td>{especialidades.usuarioRegistro}</td>
          <td>{especialidades.usuarioModificacion}</td>      
          <td>
            <button
              className="btn"
              onClick={() => navigate(`/updateEspecialidad/${especialidades.id}`)}
            >
              <span className="badge bg-success">Editar</span>
            </button>
          </td>
          <td>
            <button
              className="btn"
              onClick={() =>
                especialidades.id && handleDelete(especialidades.id)
              }
            >
              <span className="badge bg-danger">Eliminar</span>
            </button>
          </td>
        </tr>
      </tbody>
    );
}
export default EspecialidadesItem;


