import React, {useState, useEffect} from "react";

import * as EspecialidadesServer from "../../server/EspecialidadesServer";
import EspecialidadesItem from "./EspecialidadesItem";


const EspecialidaesList=()=>{
    const [especialidades, setEspecialidades] = useState([]);

    const listEspecialidades= async ()=>{
        try {
            const res = await EspecialidadesServer.listEspecialidades();
            const data = await res.json();
            setEspecialidades(data.results);
        } catch (error) {
            console.log(error);
        }            
    }

    useEffect(()=>{
        listEspecialidades();
    },[]);
    

    return (
      <div>
        <h1 className="text-center mb-4">Listado de Especialidades</h1>
        <table className="table text-center">
          <thead>
            <tr>
              <th>Id</th>
              <th>Nombres</th>
              <th>Descripcion</th>
              <th>Fecha de Registro</th>
              <th>Fecha de Modificacion</th>
              <th>Usuario Registro</th>
              <th>Usuario Modificacion</th>           
              <th>Acciones</th>
            </tr>
          </thead>
          {especialidades.map((especialidad) => (
            <EspecialidadesItem key= {especialidad.id} especialidades={especialidad} listEspecialidades= {listEspecialidades}/>
          ))}
        </table>
      </div>
    );
}
export default EspecialidaesList;

