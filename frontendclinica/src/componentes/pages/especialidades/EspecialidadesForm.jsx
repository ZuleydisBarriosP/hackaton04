import { useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";


import * as EspecialidadesServer from "../../server/EspecialidadesServer";


const EspecialidadesForm = () => {

const navigate = useNavigate();
const params = useParams();

  const initialState = {
    nombre: "",
    descripcion: "",
    fechaRegistro: "",
    fechaModificacion: "",
    usuarioRegistro: "",
    usuarioModificacion: "",
    activo : true,    
  };

  const [especialidadesForm, setEspecialidadesForm] = useState(initialState);

  const handleInputChange = (e) => {
    setEspecialidadesForm({ ...especialidadesForm, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
        let res;
        if (!params.id) {
          res = await EspecialidadesServer.registerEspecialidad(
            especialidadesForm
          );
          const data = await res.json();
          if (res.status === 201) {
            setEspecialidadesForm(initialState);
          }
        } else {
          await EspecialidadesServer.updateEspecialidad(params.id, especialidadesForm);
        }
      
      navigate("/especialidadeslist");

    } catch (error) {
      console.log(error);
    }
        
    }


    const getEspecialidad = async (especialidadId) =>{
    try{
      const res = await EspecialidadesServer.getEspecialidad(especialidadId);
      const data = await res.json();
      const { nombre, descripcion, fechaRegistro, fechaModificacion, usuarioRegistro, usuarioModificacion, activo } = data;
      setEspecialidadesForm({ nombre, descripcion, fechaRegistro, fechaModificacion, usuarioRegistro, usuarioModificacion, activo });      

    }
  catch(error){
    console.log(error);
  }
  }

  useEffect(() => {
    if (params.id) {
      getEspecialidad(params.id);
    }
  }, []);
    
 

  return (
    <>
      <h1 className="text-center mb-5">Formulario Especialidades</h1>

      <form onSubmit={handleSubmit}>
        <div className="mb-3">
          <label htmlFor="id_nombre" className="form-label">
            Nombres
          </label>
          <input
            type="text"
            className="form-control"
            id="id_nombre"
            name="nombre"
            value={especialidadesForm.nombre}
            onChange={handleInputChange}
            aria-describedby="textlHelp"
            placeholder="Ingrese nombre especialidad"
            maxLength={50}
            required=""
          />
        </div>

        <label htmlFor="id_descripcion" className="form-label">
          Descripcion
        </label>
        <input
          type="textarea"
          className="form-control"
          name="descripcion"
          value={especialidadesForm.descripcion}
          onChange={handleInputChange}
          aria-describedby="textlHelp"
          placeholder="Ingrese una descripcion"
          maxLength={250}
          required=""
          id="id_descripion"
        />

        <label htmlFor="id_fechaRegistro" className="form-label">
          Fecha de Registro
        </label>
        <input
          type="date"
          className="form-control"
          name="fechaRegistro"
          value={especialidadesForm.fechaRegistro}
          onChange={handleInputChange}
          aria-describedby="textlHelp"
          placeholder="Ingrese una Fecha de Registro"
          maxLength={50}
          required=""
          id="id_fechaRegistro"
        />

        <label htmlFor="id_fechaModificacion" className="form-label">
          Fecha Modificacion
        </label>
        <input
          type="date"
          className="form-control"
          name="fechaModificacion"
          value={especialidadesForm.fechaModificacion}
          onChange={handleInputChange}
          aria-describedby="textlHelp"
          placeholder="Ingrese Fecha Modificacion"
          maxLength={50}
          required=""
          id="id_fechaModificacion"
        />

        <label htmlFor="id_usuarioRegistro" className="form-label">
          Usuario Registro
        </label>
        <input
          type="number"
          className="form-control"
          aria-describedby="textlHelp"
          placeholder="Ingrese usuario Registro"
          name="usuarioRegistro"
          value={especialidadesForm.usuarioRegistro}
          onChange={handleInputChange}
          maxLength={50}
          required=""
          id="id_usuarioRegistro"
        />

        <label htmlFor="id_usuarioModificacion" className="form-label">
          Usuario Modificacion
        </label>
        <input
          type="number"
          className="form-control"
          aria-describedby="textlHelp"
          placeholder="Ingrese usuario Modificacion"
          name="usuarioModificacion"
          value={especialidadesForm.usuarioModificacion}
          onChange={handleInputChange}
          maxLength={50}
          required=""
          id="id_usuarioModificacion"
        />

        {params.id ? (
          <button
            type="submit"
            className="btn btn-primary mt-4 mb-4"
            defaultValue="Save"
            name="_save"
            id="id_save"
          >
            Actualizar
          </button>
        ) : (
          <button
            type="submit"
            className="btn btn-primary mt-4 mb-4"
            defaultValue="Save"
            name="_save"
            id="id_save"
          >
            Enviar
          </button>
        )}
      </form>
    </>
  );
};
export default EspecialidadesForm;
