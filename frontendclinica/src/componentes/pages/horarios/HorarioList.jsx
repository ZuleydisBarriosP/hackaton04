import React, {useState, useEffect} from "react";

import * as HorarioServer from "../../server/HorarioServer";
import HorarioItem from "./HorarioItem";


const HorariosList=()=>{
    const [horarios, setHorarios] = useState([]);

    const listHorarios= async ()=>{
        try {
            const res = await HorarioServer.listHorarios();
            const data = await res.json();
            setHorarios(data.results);
        } catch (error) {
            console.log(error);
        }            
    }

    useEffect(()=>{
        listHorarios();
    },[]);
    

    return (
      <div>
        <h1 className="text-center mb-4">Listado de horarios</h1>
        <table className="table text-center">
          <thead>
            <tr>
              <th>Id</th>
              <th>Nombre Medico</th>
              <th>Fecha de Atención</th>
              <th>Inicio de Atencion</th>
              <th>Fin de Atencion</th>
              <th>Fecha de Registro</th>
              <th>Usuario Registro</th>
              <th>Fecha de Modificacion</th>
              <th>Usuario Modificacion</th>
              <th>Acciones</th>
            </tr>
          </thead>
          {horarios.map((horario) => (
            <HorarioItem
              key={horario.id}
              horarios={horario}
              listHorarios={listHorarios}
            />
          ))}
        </table>
      </div>
    );
}
export default HorariosList;

