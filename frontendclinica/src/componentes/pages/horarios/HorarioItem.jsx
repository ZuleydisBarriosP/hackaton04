import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";


import * as HorarioServer from "../../server/HorarioServer";
import * as ClinicaServer from "../../server/ClinicaServer";




  const HorariosItem = ({horarios, listHorarios}) => {

    const navigate = useNavigate();
    const [medicos, setMedicos] = useState([]);
    

    const handleDelete = async (horarioId) => {
        await HorarioServer.deleteHorario(horarioId);
        listHorarios();
    };

  
    const listMedicos = async () => {
      try {
        const res = await ClinicaServer.listMedicos();
        const data = await res.json();
        setMedicos(data.results);
      } catch (error) {
        console.log(error);
      }
    };

    useEffect(() => {
      listMedicos();
    }, []);

    const findMedico = (medicoId) => {
      return medicos.find((medico) => medico.id === medicoId)?.nombres + " " + medicos.find((medico) => medico.id === medicoId)?.apellidos;
    }

    return (
      <tbody>
        <tr>
          <td scope="row">{horarios.id}</td>
          <td>{findMedico(horarios.medicoId)}</td>
          <td>{horarios.fechaAtencion}</td>
          <td>{horarios.inicioAtencion}</td>
          <td>{horarios.finAtencion}</td>
          <td>{horarios.fechaRegistro}</td>
          <td>{horarios.usuarioRegistro}</td>
          <td>{horarios.fechaModificacion}</td>
          <td>{horarios.usuarioModificacion}</td>
          <td>
            <button
              className="btn"
              onClick={() => navigate(`/updateHorario/${horarios.id}`)}
            >
              <span className="badge bg-success">Editar</span>
            </button>
          </td>
          <td>
            <button
              className="btn"
              onClick={() => horarios.id && handleDelete(horarios.id)}
            >
              <span className="badge bg-danger">Eliminar</span>
            </button>
          </td>
        </tr>
      </tbody>
    );
}
export default HorariosItem;


