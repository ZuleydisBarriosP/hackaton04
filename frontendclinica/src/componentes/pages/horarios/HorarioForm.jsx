import { useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";

import * as HorarioServer from "../../server/HorarioServer";
import * as ClinicaServer from "../../server/ClinicaServer";


const HorarioForm = () => {
  const navigate = useNavigate();
  const params = useParams();

  const initialState = {
    medicoId: "",
    fechaAtencion: "",
    inicioAtencion: "",
    finAtencion: "",
    activo: true,
    fechaRegistro: "",
    fechaModificacion: "",
    usuarioRegistro: "1",
    usuarioModificacion: "1",    
  };

  const [horariosForm, setHorarioForm] = useState(initialState);
    const [medicos, setMedicos] = useState([]);

  const handleInputChange = (e) => {
    setHorarioForm({ ...horariosForm, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      let res;
      if (!params.id) {
        res = await HorarioServer.registerHorario(horariosForm);
        const data = await res.json();
        if (res.status === 201) {
          setHorarioForm(initialState);
        }
      } else {
        await HorarioServer.updateHorario(params.id, horariosForm);
      }

      navigate("/horarioslist");
    } catch (error) {
      console.log(error);
    }
  };

  const getHorario = async (horarioId) => {
    try {
      const res = await HorarioServer.getHorario(horarioId);
      const data = await res.json();
      const { medicoId, fechaAtencion, inicioAtencion, finAtencion, activo, fechaRegistro, fechaModificacion, usuarioRegistro, usuarioModificacion} = data;
      setHorarioForm({
        medicoId,
        fechaAtencion,
        inicioAtencion,
        finAtencion,
        activo,
        fechaRegistro,
        fechaModificacion,
        usuarioRegistro,
        usuarioModificacion,
      });
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    if (params.id) {
      getHorario(params.id);
    }
  }, []);


      const listMedicos = async () => {
        try {
          const res = await ClinicaServer.listMedicos();
          const data = await res.json();
          setMedicos(data.results);
        } catch (error) {
          console.log(error);
        }
      };

      useEffect(() => {
        listMedicos();
      }, []);

  return (
    <>
      <h1 className="text-center mb-5">Formulario Horario</h1>

      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="">Medico</label>
          <select
            className="form-select"
            aria-label="Default select example"
            name="medicoId"
            value={horariosForm.medicoId}
            onChange={handleInputChange}
          >
            <option defaultValue="None">Selecciona un medico</option>
            {medicos.map((medico) => {
              return (
                <option key={medico.id} value={medico.id} name="medicoId">
                  {medico.nombres}
                </option>
              );
            })}
          </select>
        </div>

        <label htmlFor="id_fechaAtencion" className="form-label">
          Fecha de Atencion
        </label>
        <input
          type="date"
          className="form-control"
          name="fechaAtencion"
          value={horariosForm.fechaAtencion}
          onChange={handleInputChange}
          aria-describedby="textlHelp"
          placeholder="Ingrese una fecha de Atencion"
          maxLength={250}
          required=""
          id="id_fechaAtencion"
        />

        <label htmlFor="id_inicioAtencion" className="form-label">
          Incio de Atencion
        </label>
        <input
          type="time"
          className="form-control"
          name="inicioAtencion"
          value={horariosForm.inicioAtencion}
          onChange={handleInputChange}
          aria-describedby="textlHelp"
          placeholder="Ingrese un horario de inicio de Atencion"
          maxLength={250}
          required=""
          id="id_inicioAtencion"
        />

        <label htmlFor="id_finAtencion" className="form-label">
          Fin de Atencion
        </label>
        <input
          type="time"
          className="form-control"
          name="finAtencion"
          value={horariosForm.finAtencion}
          onChange={handleInputChange}
          aria-describedby="textlHelp"
          placeholder="Ingrese un horario de fin de Atencion"
          maxLength={250}
          required=""
          id="id_finAtencion"
        />

        <label htmlFor="id_fechaRegistro" className="form-label">
          Fecha de Registro
        </label>
        <input
          type="date"
          className="form-control"
          name="fechaRegistro"
          value={horariosForm.fechaRegistro}
          onChange={handleInputChange}
          aria-describedby="textlHelp"
          placeholder="Ingrese una Fecha de Registro"
          maxLength={50}
          required=""
          id="id_fechaRegistro"
        />

        <label htmlFor="id_fechaModificacion" className="form-label">
          Fecha Modificacion
        </label>
        <input
          type="date"
          className="form-control"
          name="fechaModificacion"
          value={horariosForm.fechaModificacion}
          onChange={handleInputChange}
          aria-describedby="textlHelp"
          placeholder="Ingrese Fecha Modificacion"
          maxLength={50}
          required=""
          id="id_fechaModificacion"
        />

        <label htmlFor="id_usuarioRegistro" className="form-label">
          Usuario Registro
        </label>
        <input
          type="number"
          className="form-control"
          aria-describedby="textlHelp"
          placeholder="Ingrese usuario Registro"
          name="usuarioRegistro"
          value={horariosForm.usuarioRegistro}
          onChange={handleInputChange}
          maxLength={50}
          required=""
          id="id_usuarioRegistro"
        />

        <label htmlFor="id_usuarioModificacion" className="form-label">
          Usuario Modificacion
        </label>
        <input
          type="number"
          className="form-control"
          aria-describedby="textlHelp"
          placeholder="Ingrese usuario Modificacion"
          name="usuarioModificacion"
          value={horariosForm.usuarioModificacion}
          onChange={handleInputChange}
          maxLength={50}
          required=""
          id="id_usuarioModificacion"
        />

        {params.id ? (
          <button
            type="submit"
            className="btn btn-primary mt-4 mb-4"
            defaultValue="Save"
            name="_save"
            id="id_save"
          >
            Actualizar
          </button>
        ) : (
          <button
            type="submit"
            className="btn btn-primary mt-4 mb-4"
            defaultValue="Save"
            name="_save"
            id="id_save"
          >
            Enviar
          </button>
        )}
      </form>
    </>
  );
};
export default HorarioForm;
