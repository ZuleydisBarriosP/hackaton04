import React, { useEffect, useState } from 'react';

//Componentes
import PacienteItem from './PacienteItem';

import * as PacienteServer from '../../server/PacienteServer'

const PacienteList = () => {
    const [pacientes,setPacientes] = useState([]);

    const listPacientes= async()=>{
        try {
            const res = await PacienteServer.listPacientes();
            const data = await res.json();            
            setPacientes(data.results)
        } catch (error) {
            console.log(error);        
        }
    }

    useEffect(()=>{
        listPacientes()},[]);

    return (
      <div>
        <h1 className="text-center mb-4">Listado pacientes</h1>

        <table className="table text-center">
          <thead>
            <tr>
              <th>Id</th>
              <th>Nombres</th>
              <th>Apellidos</th>
              <th>Dni</th>
              <th>Direccion</th>            
              <th>Sexo</th>
              <th>Fecha Nacimiento</th>  
              <th>Acciones</th>
            </tr>
          </thead>
          {pacientes.map((paciente) => (
            <PacienteItem
              key={paciente.id}
              pacientes={paciente}
              listPacientes={listPacientes}
            />
          ))}
        </table>
      </div>
    );
};

export default PacienteList;
