import { useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";

import * as PacienteServer from "../../server/PacienteServer";

const PacienteForm = () => {
const navigate = useNavigate();
const params = useParams();

console.log(params);
  const initialState = {
    nombres: "",
    apellidos: "",
    dni: "",
    direccion: "",    
    telefono: "",
    sexo: "",    
    fechaNacimiento: "",
    fechaRegistro: "",
    fechaModificacion: "",
    usuarioRegistro: "",
    usuarioModificacion: "",
    activo: true,
  };


  const [pacienteForm, setPacienteForm] = useState(initialState);

  const handleInputChange = (e) => {
    setPacienteForm({...pacienteForm, [e.target.name]: e.target.value});
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    console.log(pacienteForm);
    try {
      let res;
      if (!params.id) {
        res = await PacienteServer.registerPaciente(pacienteForm);
        const data = await res.json();
        if (res.status === 201) {
          setPacienteForm(initialState);
        }
      }else{
        await PacienteServer.updatePaciente(params.id, pacienteForm);       
      }

      navigate("/pacienteslist");
   
    } catch (error) {
      console.log(error);
    }
  }


  const getPaciente = async (pacienteId) =>{
    try{
      const res = await PacienteServer.getPaciente(pacienteId);
      const data = await res.json();
      const {nombres, apellidos, dni, direccion, telefono, sexo, fechaNacimiento, fechaRegistro, fechaModificacion, usuarioRegistro, usuarioModificacion, activo} =data;
      setPacienteForm({
        nombres,
        apellidos,
        dni,
        direccion,
        telefono,
        sexo,
        fechaNacimiento,
        fechaRegistro,
        fechaModificacion,
        usuarioRegistro,
        usuarioModificacion,
        activo,
      });
      
    }
  catch(error){
    console.log(error);
  }
  }

  useEffect(() => {
    if (params.id) {
      getPaciente(params.id);
    }
  }, []);
  


  return (
    <>
      <h1 className="text-center mb-5">Formulario Pacientes</h1>

      <form onSubmit={handleSubmit}>
        <div className="mb-3">
          <label htmlFor="id_nombres" className="form-label">
            Nombres
          </label>
          <input
            type="text"
            className="form-control"
            id="id_nombres"
            name="nombres"
            value={pacienteForm.nombres}
            onChange={handleInputChange}
            aria-describedby="textlHelp"
            placeholder="Ingrese sus nombres"
            maxLength={50}
            required=""
          />
        </div>

        <label htmlFor="id_apellidos" className="form-label">
          Apellidos
        </label>
        <input
          type="text"
          className="form-control"
          name="apellidos"
          value={pacienteForm.apellidos}
          onChange={handleInputChange}
          aria-describedby="textlHelp"
          placeholder="Ingrese sus apellidos"
          maxLength={50}
          required=""
          id="id_apellidos"
        />

        <label htmlFor="id_dni" className="form-label">
          DNI
        </label>
        <input
          type="number"
          className="form-control"
          name="dni"
          value={pacienteForm.dni}
          onChange={handleInputChange}
          aria-describedby="textlHelp"
          placeholder="Ingrese DNI"
          maxLength={50}
          required=""
          id="id_dni"
        />

        <label htmlFor="id_direccion" className="form-label">
          Direccion
        </label>
        <input
          type="text"
          className="form-control"
          name="direccion"
          value={pacienteForm.direccion}
          onChange={handleInputChange}
          aria-describedby="textlHelp"
          placeholder="Ingrese su direccion"
          maxLength={50}
          required=""
          id="id_direccion"
        />

        <label htmlFor="id_telefono" className="form-label">
          Telefono
        </label>
        <input
          type="number"
          className="form-control"
          aria-describedby="textlHelp"
          placeholder="Ingrese Telefono"
          name="telefono"
          value={pacienteForm.telefono}
          onChange={handleInputChange}
          maxLength={50}
          required=""
          id="id_telefono"
        />

        <label htmlFor="id_sexo" className="form-label">
          Sexo
        </label>
        <input
          type="text"
          className="form-control"
          name="sexo"
          value={pacienteForm.sexo}
          onChange={handleInputChange}
          aria-describedby="textlHelp"
          placeholder="Ingrese su sexo"
          maxLength={50}
          required=""
          id="id_sexo"
        />

        <label htmlFor="exampleText" className="form-label">
          Fecha Nacimiento
        </label>
        <input
          type="date"
          name="fechaNacimiento"
          value={pacienteForm.fechaNacimiento}
          onChange={handleInputChange}
          className="form-control"
          required=""
          id="id_fechaNacimiento"
        />

        <label htmlFor="exampleText" className="form-label">
          Fecha Registro
        </label>
        <input
          type="date"
          name="fechaRegistro"
          value={pacienteForm.fechaRegistro}
          onChange={handleInputChange}
          className="form-control"
          size={10}
          required=""
          id="id_fechaRegistro"
        />

        <label htmlFor="exampleText" className="form-label">
          Fecha Modificacion
        </label>
        <input
          type="date"
          className="form-control"
          name="fechaModificacion"
          value={pacienteForm.fechaModificacion}
          onChange={handleInputChange}
          size={10}
          required=""
          id="id_fechaModificacion"
        />

        <label htmlFor="exampleText" className="form-label">
          Usuario Registro
        </label>
        <input
          type="text"
          className="form-control"
          name="usuarioRegistro"
          value={pacienteForm.usuarioRegistro}
          onChange={handleInputChange}
          size={10}
          required=""
          id="id_usuarioRegistro"
        />

        <label htmlFor="exampleText" className="form-label">
          Usuario Modificacion
        </label>
        <input
          type="text"
          className="form-control"
          name="usuarioModificacion"
          value={pacienteForm.usuarioModificacion}
          onChange={handleInputChange}
          size={10}
          required=""
          id="id_usuarioModificacion"
        />

        {params.id ? (
          <button
            type="submit"
            className="btn btn-primary mt-4 mb-4"
            defaultValue="Save"
            name="_save"
            id="id_save"
          >
            Actualizar
          </button>
        ) : (
          <button
            type="submit"
            className="btn btn-primary mt-4 mb-4"
            defaultValue="Save"
            name="_save"
            id="id_save"
          >
            Enviar
          </button>
        )}
      </form>
    </>
  );
};
export default PacienteForm;
