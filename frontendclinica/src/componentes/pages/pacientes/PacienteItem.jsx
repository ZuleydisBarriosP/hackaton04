import React from 'react';
import { useNavigate } from "react-router-dom";


import * as PacienteServer from "../../server/PacienteServer";



const PacienteItem=({pacientes, listPacientes})=>{

    const navigate = useNavigate();

    const handleDelete= async (pacienteId)=>{
        await PacienteServer.deletePaciente(pacienteId);
        listPacientes();
    };
    return(
        <tbody>
                <tr>
                    <td scope="row">{pacientes.id}</td>
                    <td>{pacientes.nombres}</td>
                    <td>{pacientes.apellidos}</td>
                    <td>{pacientes.dni}</td>
                    <td>{pacientes.direccion}</td>                   
                    <td>{pacientes.sexo}</td>
                    <td>{pacientes.fechaNacimiento}</td>         
                    <td><button className="btn" onClick={()=>navigate(`/updatePaciente/${pacientes.id}`) }><span className="badge bg-success">Editar</span>
                    </button></td>
                    <td><button className="btn" onClick={()=>pacientes.id && handleDelete(pacientes.id) }><span className="badge bg-danger">Eliminar</span>
                    </button></td>
                </tr>
            </tbody>
        
    );

};
export default PacienteItem;