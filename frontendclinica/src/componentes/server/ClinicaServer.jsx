const API_URL = "http://127.0.0.1:8000/api/medicos/";


export const listMedicos = async ()=>{
    return await fetch(API_URL);
};

export const getMedico = async (medicoId)=>{
    return await fetch(`${API_URL}${medicoId}`)
}

export const registerMedico=async (newMedico)=>{
    return await fetch(API_URL,{
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(newMedico),
    });
}

export const updateMedico = async (medicoId, updatedMedico) => {
    console.log(medicoId);
    return await fetch(`${API_URL}${medicoId}/`, {
        method: "PUT",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(updatedMedico),
    });
}

export const deleteMedico = async (medicoId) => {
    return await fetch(`${API_URL}${medicoId}`, {
        method: "DELETE",
    });
};


