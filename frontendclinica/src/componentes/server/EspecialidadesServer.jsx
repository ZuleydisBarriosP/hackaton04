const API_URL = "http://127.0.0.1:8000/api/especialidades/";

export const listEspecialidades = async () => {
    return await fetch(API_URL)
};

export const getEspecialidad = async (especialidadId) => {
    return await fetch(`${API_URL}${especialidadId}`)
}

export const registerEspecialidad = async (newEspecialidad) => {
  return await fetch(API_URL, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(newEspecialidad),
  });
};

export const updateEspecialidad = async (especialidadId, updatedEspecialidad) => {
  console.log(especialidadId);
  return await fetch(`${API_URL}${especialidadId}/`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(updatedEspecialidad),
  });
};



export const deleteEspecialidad = async (especialidadId) => {
  return await fetch(`${API_URL}${especialidadId}`, {
    method: "DELETE",
  });
};
