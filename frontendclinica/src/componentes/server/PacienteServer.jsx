const API_URL = "http://127.0.0.1:8000/api/pacientes/";


export const listPacientes = async ()=>{
    return await fetch(API_URL);
};

export const getPaciente = async (pacienteId)=>{
    return await fetch(`${API_URL}${pacienteId}`)
}

export const registerPaciente=async (newPaciente)=>{
    return await fetch(API_URL,{
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(newPaciente),
    });
}

export const updatePaciente = async (pacienteId, updatedPaciente) => {
    console.log(pacienteId);
    return await fetch(`${API_URL}${pacienteId}/`, {
        method: "PUT",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(updatedPaciente),
    });
}

export const deletePaciente = async (pacienteId) => {
    return await fetch(`${API_URL}${pacienteId}`, {
        method: "DELETE",
    });
};


