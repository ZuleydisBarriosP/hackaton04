const API_URL = "http://127.0.0.1:8000/api/medicosespecialidades/";

export const listMedicoEspecialidades = async () => {
    return await fetch(API_URL)
};

export const getMedicoEspecialidad = async (medicoEspecialidadId) => {
    return await fetch(`${API_URL}${medicoEspecialidadId}`)
}

export const registerMedicoEspecialidad = async (newMedicoEspecialidad) => {
  return await fetch(API_URL, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(newMedicoEspecialidad),
  });
};

export const updateMedicoEspecialidad = async (medicoEspecialidadId, updatedMedicoEspecialidad) => {
  console.log(medicoEspecialidadId);
  return await fetch(`${API_URL}${medicoEspecialidadId}/`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(updatedMedicoEspecialidad),
  });
};



export const deleteMedicoEspecialidad = async (medicoEspecialidadId) => {
  return await fetch(`${API_URL}${medicoEspecialidadId}`, {
    method: "DELETE",
  });
};
