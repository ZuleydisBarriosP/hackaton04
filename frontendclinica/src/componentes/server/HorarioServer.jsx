const API_URL = "http://127.0.0.1:8000/api/horarios/";

export const listHorarios = async () => {
  return await fetch(API_URL);
};

export const getHorario= async (horarioId) => {
  return await fetch(`${API_URL}${horarioId}`);
};

export const registerHorario= async (newHorario) => {
  return await fetch(API_URL, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(newHorario),
  });
};

export const updateHorario= async (
  horarioId,
  updatedHorario
) => {
  console.log(horarioId);
  return await fetch(`${API_URL}${horarioId}/`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(updatedHorario),
  });
};

export const deleteHorario= async (horarioId) => {
  return await fetch(`${API_URL}${horarioId}`, {
    method: "DELETE",
  });
};
