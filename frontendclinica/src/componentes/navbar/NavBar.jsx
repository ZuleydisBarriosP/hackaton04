import { Link } from 'react-router-dom';

const NavBar = ()=> {
  return (
    <header id="header" className="fixed-top">
      <div className="container d-flex align-items-center">
        <h1 className="logo me-auto">
          <a href="#">Medilab</a>
        </h1>

        <nav id="navbar" className="navbar order-last order-lg-0">
          <ul>
            <li>
              <Link className="nav-link scrollto" to="/">
                Inicio
              </Link>
            </li>

            <li className="dropdown">
              <Link to="/medicolist">
                <span>Medicos</span> <i className="bi bi-chevron-down " />
              </Link>
              <ul>
                <li>
                  <Link to="/medicolist">Listar Medicos</Link>
                </li>
                <li>
                  <Link to="/medicosform">Agregar Medico</Link>
                </li>
              </ul>
            </li>

            <li className="dropdown">
              <Link to="/especialidadeslist">
                <span>Especialidades</span> <i className="bi bi-chevron-down" />
              </Link>
              <ul>
                <li>
                  <Link to="/especialidadeslist">Listar Especialidades</Link>
                </li>
                <li>
                  <Link to="/especialidadesform">Agregar Especialidades</Link>
                </li>
              </ul>
            </li>

            <li className="dropdown">
              <Link to="/">
                <span>Pacientes</span> <i className="bi bi-chevron-down " />
              </Link>
              <ul>
                <li>
                  <Link to="/pacienteslist">Listar Pacientes</Link>
                </li>
                <li>
                  <Link to="/pacienteform">Agregar Pacientes</Link>
                </li>
              </ul>
            </li>

            <li className="dropdown">
              <Link to="/">
                <span>Medico-Especialidades</span>{" "}
                <i className="bi bi-chevron-down " />
              </Link>
              <ul>
                <li>
                  <Link to="/medicoespecialidadeslist">Listar </Link>
                </li>
                <li>
                  <Link to="/medicoespecialidadesform">
                    Agregar Especialidad a Medico
                  </Link>
                </li>
              </ul>
            </li>

            <li className="dropdown">
              <Link to="/">
                <span>Horarios</span> <i className="bi bi-chevron-down" />
              </Link>
              <ul>
                <li>
                  <Link to="/horarioslist">Listar Horarios</Link>
                </li>
                <li>
                  <Link to="/horariosform">Agregar Horarios</Link>
                </li>
              </ul>
            </li>

            <li>
              <a className="nav-link scrollto" href="#contact">
                Contactanos
              </a>
            </li>
          </ul>
          <i className="bi bi-list mobile-nav-toggle" />
        </nav>
        <a href="#appointment" className="appointment-btn scrollto">
          <span className="d-none d-md-inline">Login</span>
        </a>
      </div>
    </header>
  );
}
export default NavBar;
