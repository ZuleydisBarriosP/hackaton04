import "bootstrap/dist/css/bootstrap.min.css";
import { Routes, Route } from "react-router-dom";
import {Formik, Field, Form} from "formik";

//Componentes
import MedicoList from './componentes/pages/medicos/MedicoList.jsx'

import TopBar from './componentes/navbar/TopBar.jsx'
import Footer from './componentes/footer/Footer.jsx'
import MedicoForm from './componentes/pages/medicos/MedicoForm';
import EspecialidaesList from './componentes/pages/especialidades/EspecialidadesList';  
import Home from "./Home.jsx";
import NavBar from "./componentes/navbar/NavBar.jsx";
import EspecialidadesForm from "./componentes/pages/especialidades/EspecialidadesForm.jsx";
import PacienteList from "./componentes/pages/pacientes/PacienteList.jsx";
import PacienteForm from "./componentes/pages/pacientes/PacienteForm.jsx";
import MedicoEspecialidadesList from "./componentes/pages/medico_especialidades/MedicoEspecialidadesList.jsx";
import MedicoEspecialidadesForm from "./componentes/pages/medico_especialidades/MedicoEspecialidadesForm.jsx";
import HorariosList from './componentes/pages/horarios/HorarioList';
import HorarioForm from './componentes/pages/horarios/HorarioForm';



function App() {
  return (
    <div>
      {/* Todolo que este aqui se ve en todas las paginas */}
      <TopBar />
      <NavBar />

      <Routes>
        <Route path="/" element={<Home />} />
      </Routes>

      <div className="container mt-5">
        <br />
        <br />
        <br />
        <br />
        <br />
        <Routes>
          <Route path="/medicolist" element={<MedicoList />} />
          <Route path="/medicosform" element={<MedicoForm />} />
          <Route path="/updateMedico/:id" element={<MedicoForm />} />

          <Route path="/especialidadeslist" element={<EspecialidaesList />} />
          <Route path="/especialidadesform" element={<EspecialidadesForm />} />
          <Route path="/updateEspecialidad/:id" element={<EspecialidadesForm />}/>

          <Route path="/pacienteslist" element={<PacienteList />} />
          <Route path="/pacienteform" element={<PacienteForm />} />
          <Route path="/updatePaciente/:id" element={<PacienteForm />}/>

          <Route path="/medicoespecialidadeslist" element={<MedicoEspecialidadesList/>} />
          <Route path="/medicoespecialidadesform" element={<MedicoEspecialidadesForm/>} />
          <Route path="/updateMedicoEspecialidad/:id" element={<MedicoEspecialidadesForm />}/>

          <Route path="/horarioslist" element={<HorariosList/>} />
          <Route path="/horariosform" element={<HorarioForm/>} />
          <Route path="/updateHorario/:id" element={<HorarioForm />}/>

        </Routes>
        
      </div>

      <Footer />
      {/* Todolo que este aqui se ve en todas las paginas */}
    </div>
  );
}

export default App
