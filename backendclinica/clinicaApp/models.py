from django.db import models
from django.contrib.auth.models import User


class Medico (models.Model):
    nombres = models.CharField(max_length=50)
    apellidos = models.CharField(max_length=50)
    dni = models.IntegerField()
    direccion = models.CharField(max_length=50)
    correo = models.CharField(max_length=50)
    telefono = models.IntegerField()
    sexo = models.CharField(max_length=10)
    numColegiatura = models.IntegerField()
    fechaNacimiento = models.DateField()
    fechaRegistro = models.DateField()
    fechaModificacion = models.DateField()
    usuarioRegistro = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='+')
    usuarioModificacion = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='+')
    activo = models.BooleanField()

    def __str__(self):
        return self.nombres + " " + self.apellidos


class Paciente (models.Model):
    nombres = models.CharField(max_length=50)
    apellidos = models.CharField(max_length=50)
    dni = models.IntegerField()
    direccion = models.CharField(max_length=50)
    telefono = models.IntegerField()
    sexo = models.CharField(max_length=10)
    fechaNacimiento = models.DateField()
    fechaRegistro = models.DateField()
    fechaModificacion = models.DateField()
    usuarioRegistro = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='+')
    usuarioModificacion = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='+')
    activo = models.BooleanField()

    def __str__(self):
        return self.nombres + " " + self.apellidos


class Especialidade (models.Model):
    nombre = models.CharField(max_length=50)
    descripcion = models.CharField(max_length=50)
    fechaRegistro = models.DateField()
    fechaModificacion = models.DateField()
    usuarioRegistro = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='+')
    usuarioModificacion = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='+')
    activo = models.BooleanField()

    def __str__(self):
        return self.nombre + " " + self.descripcion

class MedicosEspecialidade (models.Model):
    medicoId = models.ForeignKey(Medico, on_delete=models.CASCADE)
    especialidadId = models.ForeignKey(Especialidade, on_delete=models.CASCADE)
    fechaRegistro = models.DateField()
    fechaModificacion = models.DateField()
    usuarioRegistro = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='+')
    usuarioModificacion = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='+')
    activo = models.BooleanField()

    def __str__(self):
        return self.medico.nombres + " " + self.medico.apellidos + " - " + self.especialidadId.nombre


class Horario (models.Model):
    medicoId = models.ForeignKey(Medico, on_delete=models.CASCADE)
    fechaAtencion = models.DateField()
    inicioAtencion = models.TimeField()
    finAtencion = models.TimeField()
    activo = models.BooleanField()
    fechaRegistro = models.DateField()
    usuarioRegistro = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='+')
    fechaModificacion = models.DateField()
    usuarioModificacion = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='+')

    def __str__(self):
        return self.medico.nombres + " " + self.medico.apellidos + " - " + self.fechaAtencion.strftime("%d/%m/%Y")


class Cita (models.Model):
    medicoId = models.ForeignKey(Medico, on_delete=models.CASCADE)
    pacienteId = models.ForeignKey(Paciente, on_delete=models.CASCADE)
    fechaAtencion = models.DateField()
    inicioAtencion = models.TimeField()
    finAtencion = models.TimeField()
    estado = models.CharField(max_length=50)
    observaciones = models.CharField(max_length=50)
    activo = models.BooleanField()
    fechaRegistro = models.DateField()
    usuarioRegistro = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='+')
    fechaModificacion = models.DateField()
    usuarioModificacion = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='+')

    def __str__(self):
        return self.medico.nombres + " " + self.medico.apellidos + " - " + self.paciente.nombres + " " + self.paciente.apellidos + " - " + self.fechaAtencion.strftime("%d/%m/%Y")
