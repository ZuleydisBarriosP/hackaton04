from clinicaApp.views import *

from rest_framework import routers

router = routers.DefaultRouter()
router.register(r"medicos", MedicoViewSet)
router.register(r"pacientes", PacienteViewSet)
router.register(r"especialidades", EspecialidadeViewSet)
router.register(r"medicosespecialidades", MedicosEspecialidadeViewSet)
router.register(r"horarios", HorarioViewSet)
router.register(r"citas", CitaViewSet)
urlpatterns = router.urls