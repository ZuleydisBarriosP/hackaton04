from django.contrib import admin

from clinicaApp.models import *


@admin.register(Medico)
class MedicoAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombres', 'apellidos', 'dni', 'direccion',
                    'correo', 'telefono', 'sexo', 'numColegiatura', 'fechaNacimiento',
                    'fechaRegistro', 'fechaModificacion', 'usuarioRegistro',
                    'usuarioModificacion', 'activo')


@admin.register(Paciente)
class PacienteAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombres', 'apellidos', 'dni', 'direccion',
                    'telefono', 'sexo', 'fechaNacimiento', 'fechaRegistro',
                    'fechaModificacion', 'usuarioRegistro', 'usuarioModificacion',
                    'activo')


@admin.register(Especialidade)
class EspecialidadeAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre', 'descripcion',
                    'fechaRegistro', 'fechaModificacion', 'usuarioRegistro',
                    'usuarioModificacion', 'activo')


@admin.register(MedicosEspecialidade)
class MedicosEspecialidadeAdmin(admin.ModelAdmin):
    list_display = ('id', 'medicoId', 'especialidadId',
                    'fechaRegistro', 'fechaModificacion', 'usuarioRegistro',
                    'usuarioModificacion', 'activo')


@admin.register(Horario)
class HorarioAdmin(admin.ModelAdmin):
    list_display = ('id', 'medicoId', 'fechaAtencion', 'inicioAtencion',
                    'finAtencion', 'activo', 'fechaRegistro', 'usuarioRegistro', 'fechaModificacion',
                    'usuarioModificacion')


@admin.register(Cita)
class CitaAdmin(admin.ModelAdmin):
    list_display = ('id', 'medicoId',
                    'pacienteId', 'fechaAtencion', 'inicioAtencion', 'finAtencion',
                    'estado', 'observaciones',    'fechaRegistro', 'usuarioRegistro',
                    'fechaModificacion', 'usuarioModificacion')
