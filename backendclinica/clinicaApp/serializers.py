from rest_framework import serializers
from clinicaApp.models import *


class MedicoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Medico
        fields ='__all__'      



class PacienteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Paciente
        fields ='__all__'


class EspecialidadeSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Especialidade
        fields ='__all__'


class MedicosEspecialidadeSerializer(serializers.ModelSerializer):
    class Meta:
        model = MedicosEspecialidade
        fields ='__all__'


class HorarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Horario
        fields ='__all__'


class CitaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cita
        fields ='__all__'
